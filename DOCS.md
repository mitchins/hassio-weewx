# Weewx add-on

## Using with Deconz

##### Acquire an API key:

Creates a new API key which provides authorized access to the REST API.

Note The request will only succeed if the gateway is unlocked or valid HTTP basic authentification credentials are provided in the HTTP request header (see authorization).

```shell
bash-5.0# curl --header "Content-Type: application/json" \
>   --request POST \
>   --data '{"devicetype": "weewx"}' \
>   http://core-deconz:40850/api
[{"success":{"username":"30C70FAF3A"}}

bash-5.0# curl -s http://core-deconz:40850/api/30C70FAF3A/sensors/19 | jq
{
  "config": {
    "battery": 100,
    "on": true,
    "reachable": true
  },
  "ep": 1,
  "etag": "1834f2b258a8f1d0e9526969091c67e6",
  "lastseen": "2020-06-21T01:01:35.606",
  "manufacturername": "LUMI",
  "modelid": "lumi.weather",
  "name": "Multi Sensor",
  "state": {
    "lastupdated": "2020-06-21T01:01:35.606",
    "pressure": 1003
  },
  "swversion": "20161129",
  "type": "ZHAPressure",
  "uniqueid": "00:15:8d:00:02:47:6b:91-01-0403"
}
```

##### Reading material:
[https://community.home-assistant.io/t/weather-station-data/58489/59]
[https://raw.githubusercontent.com/bonjour81/station_meteo/master/weewx/driver/wxMesh.py]
[https://github.com/ovrheat/raspberry_rtl_wh1080]



# Home Assistant Community Add-on: Example

This is an example add-on for Hass.io. When started, it displays a
random quote every 5 seconds.

It shows off several features and structures like:

- Full blown GitHub repository.
- General Dockerfile structure and setup.
- The use of the `config.json` and `build.json` files.
- General shell scripting structure (`run.sh`).
- Quality assurance using CodeClimate.
- Continuous integration and deployment using CircleCI.
- Usage of the Community Hass.io Add-ons build environment.
- Small use of the Bash function library in our base images.
- The use of Docker label schema.

## Installation

The installation of this add-on is pretty straightforward and not different in
comparison to installing any other Home Assistant add-on.

1. Search for the "Example" add-on in the Supervisor add-on store and install it.
1. Start the "Example" add-on.
1. Check the logs of the "Example" add-on to see it in action.

## Configuration

Eventought this add-on is just an example add-on, it does come with some
configuration options to play around with.

**Note**: _Remember to restart the add-on when the configuration is changed._

Example add-on configuration:

### Option: `log_level`

The `log_level` option controls the level of log output by the addon and can
be changed to be more or less verbose, which might be useful when you are
dealing with an unknown issue. Possible values are:

- `trace`: Show every detail, like all called internal functions.
- `debug`: Shows detailed debug information.
- `info`: Normal (usually) interesting events.
- `warning`: Exceptional occurrences that are not errors.
- `error`:  Runtime errors that do not require immediate action.
- `fatal`: Something went terribly wrong. Add-on becomes unusable.

Please note that each level automatically includes log messages from a
more severe level, e.g., `debug` also shows `info` messages. By default,
the `log_level` is set to `info`, which is the recommended setting unless
you are troubleshooting.

## Changelog & Releases

This repository keeps a change log using [GitHub's releases][releases]
functionality. The format of the log is based on
[Keep a Changelog][keepchangelog].

Releases are based on [Semantic Versioning][semver], and use the format
of ``MAJOR.MINOR.PATCH``. In a nutshell, the version will be incremented
based on the following:

- ``MAJOR``: Incompatible or major changes.
- ``MINOR``: Backwards-compatible new features and enhancements.
- ``PATCH``: Backwards-compatible bugfixes and package updates.

## Support

Got questions?

You have several options to get them answered:

- The [Home Assistant Community Add-ons Discord chat server][discord] for add-on
  support and feature requests.
- The [Home Assistant Discord chat server][discord-ha] for general Home
  Assistant discussions and questions.
- The Home Assistant [Community Forum][forum].
- Join the [Reddit subreddit][reddit] in [/r/homeassistant][reddit]

You could also [open an issue here][issue] GitHub.

## Authors & contributors

The original setup of this repository is by [Franck Nijhof][frenck].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## License

MIT License

Copyright (c) 2017-2020 Franck Nijhof

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

