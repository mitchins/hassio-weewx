
Inside docker:
```
root@local-example:/usr/share/weewx# PYTHONPATH=/usr/share/weewx python3 user/sdr.py --help
Usage: sdr.py [--debug] [--help] [--version]
        [--action=(show-packets | show-detected | list-supported)]
        [--cmd=RTL_CMD] [--path=PATH] [--ld_library_path=LD_LIBRARY_PATH]

Actions:
  show-packets: display each packet (default)
  show-detected: display a running count of the number of each packet type
  list-supported: show a list of the supported packet types

Hide:
  This is a comma-separate list of the types of data that should not be
  displayed.  Default is to show everything.

Options:
  -h, --help            show this help message and exit
  --version             display driver version
  --debug               display diagnostic information while running
  --cmd=CMD             rtl command with options
  --path=PATH           value for PATH
  --ld_library_path=LD_LIBRARY_PATH
                        value for LD_LIBRARY_PATH
  --hide=HIDDEN         output to be hidden: out, parsed, unparsed, empty
  --action=ACTION       actions include show-packets, show-detected, list-
                        supported
```



## Allowing the USB in the container (via https://hub.docker.com/r/mikenye/piaware)
Before this container will work properly, you must blacklist the kernel modules for the RTL-SDR USB device from the host's kernel.

To do this, create a file vim  containing the following:

# Blacklist RTL2832 so docker container piaware can use the device

blacklist rtl2832
blacklist dvb_usb_rtl28xxu
blacklist rtl2832_sdr

Once this is done, you can plug in your RTL-SDR USB device and start the container.

Failure to do this will result in the error below being spammed to the container log.