docker run --rm -ti --name hassio-builder --privileged \
  -v /path/to/addon:/data -v \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  homeassistant/amd64-builder -t /data --all --test \
  -i my-test-addon-{arch} -d local